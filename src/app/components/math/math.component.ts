import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {

  calculatedValue = null;

  additionForm = new FormGroup({
    x: new FormControl(''),
    y: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  additionSubmit() {
    this.calculatedValue = null;
    this.http.get(`http://localhost:8080/math/add?x=${this.additionForm.get('x').value}
    &y=${this.additionForm.get('y').value}`).toPromise()
    .then((response) => {
      // console.log(response);
      this.calculatedValue = response;
    });
    // console.log('working');
  }

}
