import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-multiply',
  templateUrl: './multiply.component.html',
  styleUrls: ['./multiply.component.css']
})
export class MultiplyComponent implements OnInit {

  multipliedValue = null;

  multiplicationForm = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  multiplicationSubmit() {
    this.multipliedValue = null;
    const requestObject = {
      num1: this.multiplicationForm.get('num1').value,
      num2: this.multiplicationForm.get('num2').value
    }
    this.http.post(`http://localhost:8080/math/multiply`, requestObject).toPromise()
    .then((response) => {
      this.multipliedValue = response;
    });
  }

}
