import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-subtract',
  templateUrl: './subtract.component.html',
  styleUrls: ['./subtract.component.css']
})
export class SubtractComponent implements OnInit {

  subtractedValue = null;

  subtractionForm = new FormGroup ({
    g2: new FormControl(''),
    h2: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  subtractionSubmit() {
    this.subtractedValue = null;
    this.http.get(`http://localhost:8080/math/subtract/${this.subtractionForm.get('g2').value}/
    ${this.subtractionForm.get('h2').value}`).toPromise()
    .then((response) => {
      this.subtractedValue = response;
    });
  }

}
